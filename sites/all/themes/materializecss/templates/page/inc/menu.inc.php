<div id="user-account" class="black" style="">
    <span class="white-text">
    <i class="material-icons white-text">
      account_box
    </i>
    </span>
</div>
<div class="navbar-fixed" data-count="<?php print $is_new ?>">
  <nav class="transparent">
    <div class="nav-wrapper transparent">
      <!-- menu screen -->
      <?php if ($logo): ?>
        <a class="brand-logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
      <!-- menu mobile -->
      <a href="#" data-target="mobile-demo" class="sidenav-trigger">
        <i class="material-icons">menu</i>
      </a>
      <!-- lien du menu -->
      <?php if (!empty($primary_nav)): ?>
        <div class="right hide-on-med-and-down">
          <?php print render($primary_nav); ?>
        </div>
      <?php endif; ?>

    </div>
  </nav>
</div>


<!-- liste menu mobile  -->
<ul class="sidenav" id="mobile-demo">
  <?php print render($primary_nav); ?>

</ul>

<!--

<nav class="teal lighten-1" id="nav" role="navigation">
  <div class="nav-wrapper container">

    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="mdi-navigation-menu"></i></a>

  </div>
</nav> -->
