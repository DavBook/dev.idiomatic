<?php

/**
 * @file
 * Preprocess functions for page.
 */

/**
 * Implements hook_preprocess_page().
 */
function materialize_preprocess_page(&$variables) {

  global $base_path;
  global $base_url;

  // krumo($variables['node']->type);
  //

// krumo($variables['node']->type);
// krumo($variables);
// krumo($variables['node']);
//

  /////////////////////////////////////////////
  // gestion des badges news
  // gestion en jquery --> cf. custom.js
  ///////////////////////////////////////////////
  $now = new DateTime('- 1 month');
  $now = $now->format('Y-m-d H:i:s');

  $query = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('type', 'article', '=')
      ->condition('created', $now,'>');

  $count_query = $query->countQuery()
                       ->execute()
                       ->fetchField();

  $variables['is_new'] = 0;
  if($count_query > 0)
  {
    $variables['is_new'] = $count_query;
  }



  ///////////////////////////////////////////////
  // FRONT PAGE
  ///////////////////////////////////////////////
  // gestion des css / js
  if($variables['is_front'])
  {
    drupal_add_js(drupal_get_path('theme', 'materialize'). '/js/particles/particles.min.js', 'file');
    drupal_add_js(drupal_get_path('theme', 'materialize'). '/js/particles/app.js', 'file');
    drupal_add_css(drupal_get_path('theme', 'materialize'). '/css/particles.css', array('group' => CSS_THEME, 'every_page' => FALSE));
    drupal_add_css(drupal_get_path('theme', 'materialize'). '/css/particles.css', array('group' => CSS_THEME, 'every_page' => FALSE));


    $variables['menu_footer'] = menu_tree('menu-footer');



    //  = render();

    // krumo($variables['menu_footer']);

  }


  // if($variables['node']->type == 'webform')
  // {
  //   switch($variables['node']->title)
  //   {
  //     case 'Contact':
  //       $variables['theme_hook_suggestions'][] = 'page__' . 'contact';
  //     break;
  //   }
  // }
  //
  //
  // krumo($variables);
    if (isset($variables['page']['#views_contextual_links_info']))
    {
      $view_name = $variables['page']['#views_contextual_links_info']['views_ui']['view_name'];

      switch($view_name)
      {
        case 'exemple_realisation' :
        $variables['url'] = file_create_url('public://realisations.jpg');
        $variables['baseline'] = 'Look at the ring !';
        $variables['color']     = '#000000';
        $variables['position']  = 'center-align';
      }
    }


    if( isset($variables['node']) )
    {
      // pour affichage des image et titre sur page
      if( $variables['node']->type == 'article' ||
      $variables['node']->type == 'webform' ||
      $variables['node']->type == 'page' )
      {
        // pour affichage des image et titre sur page
        get_data_from_node($variables);
      }


      if($variables['node']->type == 'page_with_tab')
      {
        // pour affichage des image et titre sur page
        get_data_from_node($variables);


        if ( isset($variables['page']['content']['system_main']['nodes']) && !empty($variables['page']['content']['system_main']['nodes']) )
        {
          // gestion du menu primaire
          // récupération des informations du node
          $nodes = $variables['page']['content']['system_main']['nodes'];
          $node = array_shift($nodes);
          // formattage des données (association ancre/contenu)
          preprocess_data_for_main_menu($node, $variables);
          // application d'un théme spécifique
          // application d'un théme spécifique
          $variables['theme_hook_suggestions'][] = 'page__' . 'node' . '__primary_menu';
        }

    }


  }







  if (!empty($variables['page']['sidebar_first'])) {
    $left = $variables['page']['sidebar_first'];
  }

  if (!empty($variables['page']['sidebar_second'])) {
    $right = $variables['page']['sidebar_second'];
  }
  // Dynamic sidebars.
  if (!empty($left) && !empty($right)) {
    $variables['main_grid'] = 'col s12 m6';
    $variables['sidebar_left'] = 'col s12 m3';
    $variables['sidebar_right'] = 'col s12 m3';
  }
  elseif (empty($left) && !empty($right)) {
    $variables['main_grid'] = 'col s12 m9';
    $variables['sidebar_left'] = '';
    $variables['sidebar_right'] = 'col s12 m3';
  }
  elseif (!empty($left) && empty($right)) {
    $variables['main_grid'] = 'col s12 m9';
    $variables['sidebar_left'] = 'col s12 m3';
    $variables['sidebar_right'] = '';
  }
  else {
    $variables['main_grid'] = 'col s12';
    $variables['sidebar_left'] = '';
    $variables['sidebar_right'] = '';
  }

  // Primary nav.
  $variables['primary_nav'] = FALSE;
  if ($variables['main_menu']) {
    // Build links.
    $variables['primary_nav'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
    // Provide default theme wrapper function.
    $variables['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');
  }
  // Secondary nav.
  $variables['secondary_nav'] = FALSE;
  if ($variables['secondary_menu']) {
    // Build links.
    $variables['secondary_nav'] = menu_tree(variable_get('menu_secondary_links_source', 'user-menu'));
    // Provide default theme wrapper function.
    $variables['secondary_nav']['#theme_wrappers'] = array('menu_tree__secondary');
  }
  // Update menus in sidebars to use separate wrapper.
  materialize_iscale_page_update_sidebar_menus($variables);





















}

/**
 * Update menu if it is placed in a sidebar.
 *
 * By default Drupal renders block title above the block. It would be good to
 * render it as a menu title inside menu structure (with classes "header item").
 *
 * @param array $variables
 *   An array of variables passed from hook_preprocess_page().
 */
function materialize_iscale_page_update_sidebar_menus(&$variables) {
  $menus = menu_get_menus(TRUE);
  $sidebars = array('sidebar_first', 'sidebar_second');

  // Walk through both sidebars.
  foreach ($sidebars as $sidebar) {
    $sidebar_elements = isset($variables['page'][$sidebar]) ? element_children($variables['page'][$sidebar]) : array();
    foreach ($sidebar_elements as $element_name) {
      $element = $variables['page'][$sidebar][$element_name];

      // Check if sidebar element is a menu.
      if (isset($element['#block']) &&
          in_array($element['#block']->module, array('system', 'menu')) &&
          array_key_exists($element['#block']->delta, $menus)) {

        // Do not add title item if it is empty.
        if (!empty($variables['page'][$sidebar][$element_name]['#block']->subject)) {
          $menu_header['menu_header'] = array(
            '#prefix' => '<h4 class="collection-header">',
            '#suffix' => '</h4>',
          );
          $menu_header['menu_header']['header'] = array(
            '#markup' => $variables['page'][$sidebar][$element_name]['#block']->subject,
          );

          $variables['page'][$sidebar][$element_name] = $menu_header + $variables['page'][$sidebar][$element_name];
        }

        $variables['page'][$sidebar][$element_name]['#is_menu'] = TRUE;
      }
    }
  }
}








/**
 * Pour les pages du menu principal
 * formattage des données pour rendu dynamique des tabs
 *  - gestion des ancres
 *  - gestion des contenus associés
 * formattage du menu principal // gestion des tab --> cf. materialize css API
 *  - Redéfinisstaion des titre de liens
 *  - Redéfinition de lien href
 *
 *  pour le traitement automatique, il faut associer les ancres et les textes dans le même ordre (cf. création des pages dans l'administration)
 *
 * @param  array $node          le node contenu dans la page
 * @param  array $variables     les variables de la page
 */
function preprocess_data_for_main_menu($node, &$variables)
{
  // déclaration des tabulations du menu primaire
  $variables['tabs_primary']['#theme'] =  'menu_local_tasks';

  // récupération des informations
  $content  = $node['field_pwt_content'];

  foreach ($content as $key => $item)
  {
    // cf. structuration du node
    if ( is_int($key) )
    {
      // récupération des informations
      $datas = $item['entity']['field_collection_item'];

      foreach($datas as $data){
        $title = $data['field_pwt_titre']['#items'][0]['value'];
        $body = $data['field_pwt_description']['#items'][0]['value'];
      }

      // formattage de l'ancre
      $id = stringToUrl($title);
      $anchor = '#'. $id;

      // stockage des données
      $render[] =array(
            'title' => $title,
            'body'  => $body,
            'id'    => $id,
            'ancre' => $anchor
          );

      // alimentation du menu primaire
      $variables['tabs_primary']['#primary'][$key]['#theme']['href']  = 'menu_local_task';
      $variables['tabs_primary']['#primary'][$key]['#link']['href']   = $anchor;
      $variables['tabs_primary']['#primary'][$key]['#link']['title']  = $title;
    }
  }

  // données à passer à la page
  $variables['render'] = $render;
}








function get_data_from_node(&$variables)
{
  // gestion de l'image à la une
  $uri = $variables['node']->field_content_image[LANGUAGE_NONE][0]['uri'];
  $url = file_create_url($uri);
  $variables['url'] = $url;

  $variables['title'] = $variables['node']->title;
  $variables['baseline'] = $variables['node']->field_baseline[LANGUAGE_NONE][0]['value'];
  $variables['color']     = $variables['node']->field_color[LANGUAGE_NONE][0]['rgb'];
  $variables['position']  = $variables['node']->field_title_position[LANGUAGE_NONE][0]['value'];

}





















/**
 * Formatte une chaine de caractère pour devenir une url
 *
 * @param  string  $string        la chaine à formatter
 * @return string $url            la chaine formattée en kebab case
 */
function stringToUrl($string) {

  // on convertit la chaîne en majuscule
	$url = mb_strtoupper($string , "UTF-8");

  // suppression des accents
	$url  = str_replace(
            array('Â','Ä','À','Ç','È','É','Ê','Ë','ŒÎ','Ï','Ô','Ö','Ù','Û','Ü'),
            array('A','A','A','C','E','E','E','E','I','I','O','O','U','U','U' ),
            $url
          );

  // suppression de tous les caractères qui ne sont pas des caractères ou des tirets
	$url = preg_replace('`[^A-Z[:space:]\'0-9-]`', '', $url);

  // on remplace les espaces
	$url = preg_replace('`[[:space:]\']{1,}`', '-', trim($url));

	// on repasse tout en minuscule
	$url = strtolower($url);

	return $url;
}
