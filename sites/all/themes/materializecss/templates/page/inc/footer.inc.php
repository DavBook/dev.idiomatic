<footer class="page-footer blue accent-2">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">Nous contacter</h5>
        <p class="grey-text text-lighten-4">
          Nous sommes basés à Nantes. Nous travaillons dans tout le grand ouest mais également dans la région parisienne et lyonnaise ... Et partout dans le monde =)
        </p>
        <p class="grey-text text-lighten-4">
          <i class="material-icons">contact_phone</i>
          06 59 45 56 54
        </p>
        <p class="grey-text text-lighten-4">
          <i class="material-icons">contact_mail</i>
          david@idiomatic.fr
        </p>
      </div>
      <div class="col l4 offset-l2 s12">
        <h5 class="white-text">Plus d'informations</h5>
        <!-- <ul>
          <li><a class="grey-text text-lighten-3" href="#!">Fonctionnement du blog</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Politique des données</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Conditions générales de ventes</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Mentions legales</a></li>
        </ul> -->
        <?php print render($menu_footer); ?>


      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      Made with <i class="material-icons">favorite</i> by idiomatic
      <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
    </div>
  </div>
</footer>
