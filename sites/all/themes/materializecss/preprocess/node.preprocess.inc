<?php

/**
 * @file
 * Contains preprocess functions for node.
 */

/**
 * Implements hook_preprocess_node().
 */
function materialize_preprocess_node(&$variables) {

  // krumo($variables['field_tags']);
  //
  if($variables['type'] == 'article' || $variables['type'] == 'page' )
  {
    // gestion du submitted
    $variables['submitted'] =   date("l, M jS, Y", $variables['created']);


    // gestion du chip information de publication
    $fid  = $variables['picture'];
    $file = file_load($fid);
    $uri  = $file->uri;
    $url  = file_create_url($uri);
    $variables['url'] = $url;

    // gestion des tags
    $variables['tags'] = array();
    foreach($variables['field_tags'] as $tag)
    {
      $variables['tags'][] = $tag['taxonomy_term']->name;
    }

    $variables['theme_hook_suggestions'][] = 'node__' . 'content';

  }







  /////////////////////////////////////////////////////////////
  // EXEMPLE DE REALISATION
  /////////////////////////////////////////////////////////////
  if($variables['type'] == 'realisation' )
  {
    // image
    $uri = $variables['node']->field_create_image[LANGUAGE_NONE][0]['uri'];
    $url = file_create_url($uri);
    $variables['url'] = $url;

    // gestion des caractéristiques techniques
    foreach( $variables['field_create_techno'] as $techno)
    {
      $name = $techno['taxonomy_term']->name;
      $uri =  'public://' . strtolower($name) . '.png';
      $url = file_create_url($uri);

      $variables['technos'][$name] = $url;
    }

    // gestion des spécification
    $variables['spec'] =  isset($variables['body'][0]['safe_value']) ?  $variables['body'][0]['safe_value'] : '';

    $variables['theme_hook_suggestions'][] = 'node__' . 'realisation' . '__full';
  }
  // fin realisation





}
