<?php

/**
 * @file
 * Materialize theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php
  $links = render($content['links']);
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> card"<?php print $attributes; ?>>







  <!-- <div class="card"> -->

    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="<?php print $url ?>">
    </div>

    <div class="card-content red darken-4">
      <span class="card-title activator white-text">
        <?php print $title; ?>
        <i class="material-icons right">more_vert</i>
      </span>
      <p>
        <a href="<?php print $node->field_create_url[LANGUAGE_NONE][0]['safe_value']; ?>" target="_blank">
            <span class="white-text">
              Visiter le site
              <i class="material-icons">more_horiz</i>
            </span>
        </a>
      </p>
    </div>

    <div class="card-reveal grey ">
      <!-- icone close  -->
      <div class= "row">
        <span class="card-title grey-text text-darken-4">
          <i class="material-icons right">close</i>
        </span>
      </div>

      <!-- spécification -->
      <?php if($spec != '') : ?>
      <div class= "row">
        <div class="col s12 m2">
          <span>
            <i class="material-icons">attachment</i>
          </span>
        </div>
        <div class="col s12 m10">
          <span class="card-title">
            Spécifications
          </span>
          <?php print $spec; ?>
        </div>
      </div>
      <?php endif; ?>
      <!-- fin spécification -->


      <!-- caractéristique techniques -->
      <div class= "row ">
        <div class="col s12 m2">
          <span>
            <i class="material-icons">build</i>
          </span>
        </div>
        <div class="col s12 m10">
          <span class="card-title">
            Caractéristiques techniques
          </span>
          </br>
          <?php foreach($technos as $name => $url) : ?>
          <img class="responsive tooltipped" style="height:50px;width:50px;"  data-position="bottom" data-tooltip="<?php print $name; ?>" src="<?php print $url; ?>">
          <?php endforeach; ?>
        </div>
      </div>
      <!-- fin caractéristique techniques -->


    </div>
    <!-- fin card reveal -->
  <!-- </div> -->
</div>
