/**
 * @file
 * Contains helper functions to work with theme.
 */

(function ($) {
  "use strict"

  /**
   * Changes caret icon for fieldset when it is being collapsed or expanded.
   */
  Drupal.behaviors.materializeFieldset = {
    attach: function (context) {
      jQuery('fieldset', context).on('collapsed', function (data) {
        var $caretIcon = jQuery('.fieldset-title i', this);
        if (data.value === true) {
          $caretIcon.removeClass('mdi-hardware-keyboard-arrow-down').addClass('mdi-hardware-keyboard-arrow-right');
        }
        else {
          $caretIcon.removeClass('mdi-hardware-keyboard-arrow-right').addClass('mdi-hardware-keyboard-arrow-down');
        }
      });
    }
  };
  jQuery(".button-collapse").sidenav();
})(jQuery);


jQuery(document).ready(function(){

  // Plugin initialization
 jQuery('.slider').slider({
    fullWidth: true,
    indicators: false
  });

  jQuery('.tooltipped').tooltip();
  jQuery('.tabs').tabs();


  // gestion du bandeau de connexion
  jQuery( window ).scroll(function() {
    console.log(jQuery(this).scrollTop())
    if( jQuery(this).scrollTop() > 0)
    {
      jQuery(  "#user-account" ).css( "display", "none" );
    }
    else
    {
      jQuery( "#user-account" ).css( "display", "block" );
    }

  });

  // badge new
  var nb = jQuery('div.navbar-fixed').attr('data-count');
  jQuery('ul.menu a[href="/node/1"]').append('<span class="new badge">' + nb + '</span>');







});
