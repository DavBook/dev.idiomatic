<?php
/**
 * @file
 * Materialize theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>


<div id="page">


  <!-- navbar -->
  <?php include 'inc/menu.inc.php' ?>

  <?php if (!empty($page['header'])): ?>
    <div class="top">
      <?php print render($page['header']); ?>
    </div>
  <?php endif; ?><!-- /.header  -->


  <div class="row page grid">


    <!-- Slider  -->
      <div class="col s12">
          <div class="slider">
              <ul class="slides">
                <li>
                  <div id="particles-js"></div>
                  <div  class=" caption center-align">
                    <span class="black-text"><h3>Question techno, on déboite</h3></span>
                    <span class="black-text"><h5>On a des beaux ordinateurs</h5></span>
                    <span class="black-text"><a class="btn waves-effect white grey-text darken-text-2">button</a>
                  </div>
                </li>
                <li>
                  <div id="particles-js-2"></div>
                  <div class=" caption center-align">
                    <span class="black-text"><h3>On fait tout sur mesure</h3></span>
                    <span class="black-text"><h5>Parlez en à ma femme, elle est couturière</h5></span>
                    <a class="btn waves-effect white grey-text darken-text-2">button</a>
                  </div>
                </li>
                <li>
                  <div id="particles-js-3"></div>
                  <div class=" caption center-align">
                    <span class="black-text"><h3>Notre équipe est très motivé</h3></span>
                    <span class="black-text"><h5>On adore quand un plan se déroule sans accroc</h5></span>
                    <span class="black-text"><a class="btn waves-effect white grey-text darken-text-2">button</a>
                  </div>
                </li>
              </ul>
            </div>
          </div>


      <div class="container">

        <div class="row">
          <div class="col s4">
            <div class="center">
              <h1>
                <i title="icon-emo-happy" class="icon fontello icon-emo-happy" aria-hidden="true"></i>
              </h1>
              <p>Nam de isto magna dissensio est. Nobis aliter videtur, recte secusne, postea; Sed tamen intellego quid velit. Praeteritis, inquit, gaudeo. Quid autem habent admirationis, cum prope accesseris? Scrupulum, inquam, abeunti; An eiusdem modi? </p>

              <a class="waves-effect waves-light blue accent-2 btn-large">
                <i class="material-icons right">more_horiz</i>
                En savoir plus
              </a>
            </div>
          </div>

          <div class="col s4">
            <div class="center">
              <h1>
                <i title="icon-emo-grin" class="icon fontello icon-emo-grin" aria-hidden="true"></i>
              </h1>
              <p>Nam de isto magna dissensio est. Nobis aliter videtur, recte secusne, postea; Sed tamen intellego quid velit. Praeteritis, inquit, gaudeo. Quid autem habent admirationis, cum prope accesseris? Scrupulum, inquam, abeunti; An eiusdem modi? </p>

              <a class="waves-effect waves-light blue accent-2 btn-large">
                <i class="material-icons right">more_horiz</i>
                En savoir plus
              </a>
            </div>
          </div>

          <div class="col s4">
            <div class="center">
              <h1>
                <i title="icon-emo-surprised" class="icon fontello icon-emo-surprised" aria-hidden="true"></i>
              </h1>
              <p>Nam de isto magna dissensio est. Nobis aliter videtur, recte secusne, postea; Sed tamen intellego quid velit. Praeteritis, inquit, gaudeo. Quid autem habent admirationis, cum prope accesseris? Scrupulum, inquam, abeunti; An eiusdem modi? </p>

              <a class="waves-effect waves-light blue accent-2 btn-large">
                <i class="material-icons right">more_horiz</i>
                En savoir plus
              </a>
            </div>
          </div>
        </div>

        <div class="divider"></div>
        <div class="section">
          <h1>Réalisations</h1>
            <p>
              <?php print views_embed_view('exemple_realisation', $display_id = 'default') ?>
            </p>
          </div>
        </div>

      </div>


    </div>
  </div>



  <!-- footer -->
  <?php include 'inc/footer.inc.php' ?>

</div> <!-- /#page -->
